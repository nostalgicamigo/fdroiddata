Categories:Navigation
License:GPLv3
Web Site:
Source Code:https://github.com/mapsquare/osm-contributor
Issue Tracker:https://github.com/mapsquare/osm-contributor/issues

Auto Name:Osm Contributor
Summary:OpenStreetMap Contributor Mapping Tool
Description:
The Openstreetmap Contributor app allows anyone to contribute to OSM. It enables
those-who-know to easily manage a group of newbies as the contribution process
is intuitive and easy. The App comes in three flavours: store (for the Android
Store version), poi-storage (for MapSquare POI Databases), and template (for the
osm.mapsquare.io tool for Mapping parties). Bring your MapParties to a whole new
level!
.

Repo Type:git
Repo:https://github.com/mapsquare/osm-contributor

Build:1.3.0,4
    disable=crashlytics
    commit=372cd9c8f73de631357bd3d902ef42e5ac9d823f
    gradle=yes
    srclibs=NoAnalytics@af5e6573bf5e221c66a0ee3ea45337fa05e812d9
    prebuild=cp -fR $$NoAnalytics$$/src/com src/ && \
        sed --in-place /play-services/d build.gradle

Maintainer Notes:
See https://github.com/mapsquare/osm-contributor/issues/1 for request to
support a Gradle flavour which excludes crashlytics and play services.
.

Auto Update Mode:None
# Auto Update Mode:Version %v
Update Check Mode:Tags
Current Version:Unknown
Current Version Code:14
