Categories:Theming
License:MIT
Web Site:http://alexanderfedora.blogspot.com
Source Code:https://github.com/ghisguth/sunlight/tree/HEAD/blurredlines
Issue Tracker:https://github.com/ghisguth/sunlight/issues

Name:Blurred Lines
Auto Name:Blurred Lines Live Wallpaper
Summary:Live wallpaper
Description:
This is small live wallpaper inspired by 1K demo [http://www.tylerdurden.net.ru
"In the mist of web"]. Website is in Russian.
.

Repo Type:git
Repo:https://github.com/ghisguth/sunlight.git

# old repo
Build:1.1-2,9
    commit=4310949

Build:1.2,10
    commit=9457a7ebd586b6aa00159fd1c7ac11852a88c38a
    subdir=blurredlines

Build:1.3,11
    commit=b1fc0965aed6645e11f5da1d3d4199f8240dd68e
    subdir=blurredlines
    gradle=yes

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:1.3
Current Version Code:11
