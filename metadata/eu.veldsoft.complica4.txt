AntiFeatures:Ads
Categories:Games
License:GPLv3
Web Site:
Source Code:https://github.com/TodorBalabanov/Complica4
Issue Tracker:https://github.com/TodorBalabanov/Complica4/issues

Auto Name:Complica4
Summary:Puzzle game
Description:
Based on the original board game Complica.
.

Repo Type:git
Repo:https://github.com/TodorBalabanov/Complica4

Build:1.0,1
    commit=186e5f3ba0f61f549fae7a43265c5bdacf8fd4ad

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:1.0
Current Version Code:1
